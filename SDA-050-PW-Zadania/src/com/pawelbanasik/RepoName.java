package com.pawelbanasik;

public class RepoName {
	private String name;
	private String secondName;

	public RepoName() {
	}

	public RepoName(String name, String secondName) {
		super();
		this.name = name;
		this.secondName = secondName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	public String getRepoName(String name, String secondName) {
		
			String myName = this.name.toLowerCase(), mySecondName = this.secondName.toLowerCase();
			int index = 3;
			if (myName.charAt(2) == mySecondName.charAt(0)) {
				index++;
			}
			return myName.substring(0, 1).toUpperCase()
					+ myName.substring(1, index).concat(mySecondName.substring(0, 1).toUpperCase())
							.concat(mySecondName.substring(1, index)).concat("JA2");
		
	}

}

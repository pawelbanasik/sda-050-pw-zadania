package com.pawelbanasik;
public class Main {

	public static void main(String[] args) {

		/*
		String str1 = "a";
		String str2 = "b";
		
		System.out.println(str1 == str2);
		
		// equals
		System.out.println(str1.equals(str2));
		
		// equalsIgnoreCase
		System.out.println(str1.equalsIgnoreCase("A"));
		
		// length
		System.out.println(str1.length());
		
		// substring
		str1 = "oksymoron";
		System.out.println(str1.substring(0, 3));
		
		// O K S Y M O R O N
		//0 1 2 3 4 5 6 7 8 9
		System.out.println(str1.substring(3)); //
		
		// trim - z przodu i z konca
		str1 = "    Pawel   ";
		System.out.println(str1);
		System.out.println(str1.trim());
		
		// charAt
		str1 = "test";
		System.out.println(str1.charAt(1)); // bedzie e
		
		// replace
		System.out.println(str1.replace("t", "_"));
		
		// concat
		System.out.println(str1.concat(str2)); // zamiast plusa
		
		// contains, startsWith, endsWith
		System.out.println(str1.startsWith("te")); // boolean
		System.out.println(str1.endsWith("t")); // konczy sie na, boolean
		System.out.println(str1.contains("es")); // sprawdza czy jest w srodku boolean
		
		// index of, lastIndexOf
		str1 = "Na polanie sa Polanie"; // 'ni'
		System.out.println(str1.lastIndexOf("ni")); // ostatnie wystapienie
		System.out.println(str1.indexOf("ni")); // pierwsze wystapienie
		
		// toUpperCase, toLowerCase
		System.out.println(str1.toUpperCase());
		System.out.println(str1.toLowerCase());
		
		// split - rozdziela znak wg znaku
		str1 = "oksymoron";
		// {"", "ksym", "r", "n"} 
		String[] arrOfStrings = str1.split("o");
		for (String s: arrOfStrings) {
			System.out.println(s); //print bez ln zrobil i wtedy w jednej linii
		}
		
		RepoName repoName = new RepoName();
		System.out.println(repoName.getRepoName("Pawel", "Walnicki"));
		
		*/
		
	
		
	
		new StringUtil("Pawel").letterSpacing().print(); 
		new StringUtil("Pawel").reverse().print(); 
		new StringUtil("Pawel").getAlphabet().print(); 
		new StringUtil("Pawel").getFirstLetter().print(); 
		new StringUtil("Pawel").limit(2).print(); 
		new StringUtil("Pawel").insertAt("xyz", 2).print(); 
		new StringUtil(null).readText().print(); 
		new StringUtil("Pawel").resetText().print(); 
		
		
		
		
		
		

		
	}

}

package com.pawelbanasik;

import java.util.Scanner;

public class StringUtil {

	private String param;

	public StringUtil(String param) {
		super();
		this.param = param;
	}

	public StringUtil letterSpacing() {

		String tmp = "";
		for (int i = 0; i < this.param.length(); i++) {
			tmp += this.param.charAt(i) + " ";
		}
		this.param = tmp.substring(0, tmp.length() - 1);
		return this; // chaining
	}

	public StringUtil reverse() {
		String tmp = "";
		for (int i = this.param.length()-1; i >= 0; i--) {

			tmp += this.param.charAt(i) + "";
		}
		this.param = tmp;
		return this;
	}

	public StringUtil getAlphabet() {

		this.param = ""; // zerujemy go
		for (char c = 97; c < 123; c++) {
			this.param += c + ""; // rzutowanie w sposob
		}
		return this;
	}

	public StringUtil getFirstLetter() {

		this.param = this.param.charAt(0) + "";

		return this;
	}

	public StringUtil limit(int n) {

		this.param = this.param.substring(n);
		return this;
	}

	public StringUtil insertAt(String string, int n) {
		this.param = this.param.substring(0, n) + string + this.param.substring(n);
		return this;
	}

	public StringUtil readText() {
		System.out.println("Wpisz wartosc do konsoli: ");
		Scanner scanner = new Scanner(System.in);
		this.param = scanner.next();
		scanner.close();
		return this;

	}

	public StringUtil resetText() {

		for (int i = 0; i < param.length(); i++) {
			this.param = param.replace(param.charAt(i) + "", " "); // castowanie
																	// myk
		}
		return this;
	}
	
	public void print() {
		
		System.out.println(this.param);
	}
}
